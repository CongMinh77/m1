# A Successful Git Branching Model

### master

```
Là nhánh chính và duy nhất
Sử dụng cho môi trường Production
Thay đổi được merge vào master và sẽ có 1 phiên bản production mới được release
```

### develop

```
Là nhánh chính
Sử dụng cho môi trường Development
Phản ánh trạng thái thay đổi trong quá trình phát triển
```

### Feature branches: Sử dụng để phát triển tính năng mới

- Tách từ develop
- Merge vào develop
- Đặt tên: feature/**

### Release branches: Được sử dụng để chuẩn bị release 1 production mới

- Tách từ: develop
- Merge vào: develop và master
- Đặt tên: release/date_release
 
### Hotfix branches: Sử dụng để giải quyết bugs nghiêm trọng trên Production

- Tách từ: master
- Merge vào: develop và master
- Đặt tên: hotfix/**
 
### Bugfix branches: Để fix bugs trong quá trình phát triển tính năng hoặc release

- Tách từ: develop hoặc release
- Merge vào: develop và master
- Đặt tên: bugfix/**


# Git workflow

- Tạo branch mới từ Develop (X)
- Coding + commit code
- Pull code từ origin Develop về nhánh (X)
- Fix conflict nếu có
- Push X lên origin X
- Tạo merge request
